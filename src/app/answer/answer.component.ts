import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-answer',
  templateUrl: './answer.component.html',
  styleUrls: ['./answer.component.css']
})
export class AnswerComponent implements OnInit {
  username = '';
  isButtonEnabled = false;
  buttonLabel = 'Loading...';

  constructor() {
    this.doButtonDisable();
  }

  ngOnInit(): void {
  }

  onUpdateUsername(event: Event) {
    // this.username = (<HTMLInputElement>event.target).value;
    if (this.username) {
      this.doButtonEnable();
    } else {
      this.doButtonDisable();
    }
  }

  onButtonClick() {
    this.doReset();
  }

  doReset() {
    this.username = '';
    this.doButtonDisable();
  }

  doButtonEnable() {
    this.isButtonEnabled = true;
    this.buttonLabel = 'Enabled';
  }

  doButtonDisable() {
    this.isButtonEnabled = false;
    this.buttonLabel = 'Disabled';
  }
}
